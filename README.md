We specialize in TVs ranging from 43" to 98", reclining seating, all-wood TV consoles and entertainment centers, and soundbars and surround sound systems from Bose and Samsung. Customer service "like the old days," and pricing that beats Best Buy, Amazon, and Warehouse clubs. Visit us!

Address: 1030 Baltimore Blvd, Westminster, MD 21157, USA

Phone: 410-871-2601

Website: https://www.thebigscreenstore.com